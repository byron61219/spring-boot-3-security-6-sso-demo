package net.arthuang.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PageController {

	private final static Logger logger = LogManager.getLogger(PageController.class);

	@GetMapping("/")
	public String root(
			Model model,
			@AuthenticationPrincipal OidcUser user) {

		if (user != null) {
			model.addAttribute("name", user.getName());
		} else {
			model.addAttribute("name", "nologin");
			logger.error("user is null");
		}

		return "index";
	}

	@GetMapping("/index")
	public String index() {
		return "index";
	}

	@GetMapping("/list")
	public String list() {
		return "list";
	}

	@GetMapping("/login")
	public String login() {
		return "login";
	}
}
