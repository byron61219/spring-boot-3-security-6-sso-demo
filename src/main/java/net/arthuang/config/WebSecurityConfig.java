package net.arthuang.config;

import java.util.HashSet;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;
import org.springframework.security.oauth2.core.oidc.IdTokenClaimNames;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;

@Configuration
@EnableWebSecurity(debug = false)
public class WebSecurityConfig {

	private final static Logger logger = LogManager.getLogger(WebSecurityConfig.class);

	@Bean
	SecurityFilterChain webFilterChain(HttpSecurity http) throws Exception {
		http
				.authorizeHttpRequests(
						auth -> auth
								.requestMatchers(
										"/**", "/js/**", "/css/**", "/image/**",
										"/index", "/login")
								.permitAll()
								.anyRequest().authenticated())

				.oauth2Login(login -> login
						.loginPage("/login").permitAll()
						.userInfoEndpoint(
								point -> point
										.oidcUserService(oidcUserService())))

				.sessionManagement(
						session -> session.sessionCreationPolicy(
								SessionCreationPolicy.STATELESS))

				.securityContext(security -> security
						.securityContextRepository(new HttpSessionSecurityContextRepository()))

		;

		return http.build();
	}

	@Bean
	OAuth2UserService<OidcUserRequest, OidcUser> oidcUserService() {
		return oidcUserRequest -> {
			OidcUserService delegate = new OidcUserService();
			OidcUser oidcUser = delegate.loadUser(oidcUserRequest);

			logger.info("oidcUser.getSubject(): {}", oidcUser.getSubject());
			logger.info("oidcUser.getFullName(): {}", oidcUser.getFullName());
			logger.info("oidcUser.getGivenName(): {}", oidcUser.getGivenName());
			logger.info("oidcUser.getFamilyName(): {}", oidcUser.getFamilyName());
			logger.info("oidcUser.getMiddleName(): {}", oidcUser.getMiddleName());
			logger.info("oidcUser.getNickName(): {}", oidcUser.getNickName());
			logger.info("oidcUser.getPreferredUsername(): {}", oidcUser.getPreferredUsername());
			logger.info("oidcUser.getProfile(): {}", oidcUser.getProfile());
			logger.info("oidcUser.getPicture(): {}", oidcUser.getPicture());
			logger.info("oidcUser.getWebsite(): {}", oidcUser.getWebsite());
			logger.info("oidcUser.getEmail(): {}", oidcUser.getEmail());
			logger.info("oidcUser.getEmailVerified(): {}", oidcUser.getEmailVerified());
			logger.info("oidcUser.getGender(): {}", oidcUser.getGender());
			logger.info("oidcUser.getBirthdate(): {}", oidcUser.getBirthdate());
			logger.info("oidcUser.getZoneInfo(): {}", oidcUser.getZoneInfo());
			logger.info("oidcUser.getLocale(): {}", oidcUser.getLocale());
			logger.info("oidcUser.getPhoneNumber(): {}", oidcUser.getPhoneNumber());
			logger.info("oidcUser.getPhoneNumberVerified(): {}", oidcUser.getPhoneNumberVerified());

			Set<GrantedAuthority> mappedAuthorities = new HashSet<>();
			mappedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));

			return new DefaultOidcUser(mappedAuthorities, oidcUser.getIdToken(), oidcUser.getUserInfo());
		};
	}

	@Bean
	public ClientRegistrationRepository clientRegistrationRepository() {
		return new InMemoryClientRegistrationRepository(
				this.googleClientRegistration(),
				this.lineClientRegistration());
	}

	private ClientRegistration googleClientRegistration() {
		return ClientRegistration.withRegistrationId("google")
				.clientId("<your client id>")
				.clientSecret("<your client secret>")

				// as same as my register callback
				.redirectUri("{baseUrl}/login/oauth2/code/{registrationId}")

				.clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)

				.scope("openid", "profile", "email")
				.authorizationUri("https://accounts.google.com/o/oauth2/v2/auth")
				.jwkSetUri("https://www.googleapis.com/oauth2/v3/certs")
				.tokenUri("https://oauth2.googleapis.com/token")
				.userInfoUri("https://www.googleapis.com/oauth2/v3/userinfo")
				.userNameAttributeName(IdTokenClaimNames.SUB)
				.clientName("Google")
				.build();
	}

	private ClientRegistration lineClientRegistration() {
		return ClientRegistration.withRegistrationId("line")
				.clientId("<your client id>")
				.clientSecret("<your client secret>")

				// as same as my register callback
				.redirectUri("{baseUrl}/login/oauth2/code/{registrationId}")

				.clientAuthenticationMethod(ClientAuthenticationMethod.CLIENT_SECRET_BASIC)
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)

				.scope("openid", "profile", "email")
				.authorizationUri("https://access.line.me/oauth2/v2.1/authorize")
				.jwkSetUri("https://api.line.me/oauth2/v2.1/certs")
				.tokenUri("https://api.line.me/oauth2/v2.1/token")
				.userInfoUri("https://api.line.me/oauth2/v2.1/userinfo")
				.userNameAttributeName(IdTokenClaimNames.SUB)
				.clientName("LINE")
				.build();
	}

}
